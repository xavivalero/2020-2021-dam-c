package A3Bloc1;

/*IES Sabadell.
CFGS DAM M03 UF1
Bloc 1 Exercici 7
Descripcio: Algorisme que llegeix dos n�meros i calcula la suma, la resta, la multiplicaci� i la divisi� reals.
Autor: David L�pez 
 */

import java.util.Scanner;

public class Exercici7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner reader = new Scanner(System.in);
		int num1 = 0;
		int num2 = 0;
		
		System.out.print("Introdueix un numero: ");
		num1 = reader.nextInt();
		
		System.out.print("Introdueix un altre numero: ");
		num2 = reader.nextInt();
		
		reader.close();
		
		int suma = num1+num2;
		int resta = num1-num2;
		int mult = num1*num2;
		int div = num1/num2;
		int residu = num1%num2;
		
		System.out.println("sumats: "+suma);
		System.out.println("restats: "+resta);
		System.out.println("multiplicats: "+mult);
		System.out.println("dividits: "+div+" residu: "+residu);
	}

}
