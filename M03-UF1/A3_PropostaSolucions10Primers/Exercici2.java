package A3Bloc1;

/*IES Sabadell.
CFGS DAM M03 UF1
Bloc 1 Exercici 2
Descripcio: Algorisme que llegeix un n�mero i compara si �s major que 10.
Autor: David L�pez 
 */

import java.util.Scanner;

public class Exercici2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner reader = new Scanner(System.in);
		int num = 0;
		
		System.out.print("Escriu un numero per comprovar si es major que 10: ");
		num = reader.nextInt();
		
		reader.close();
		
		if (num>10) {
			System.out.print("El "+num+" es MAJOR que 10");
		}
		else {
			if (num<10) { 
				System.out.print("El "+num+" es MENOR que 10");
			}
			else {
				if (num==10) {
					System.out.print("El "+num+" es IGUAL que 10");
				}
			}
			
		}
				
	}

}
