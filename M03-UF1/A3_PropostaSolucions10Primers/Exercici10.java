package A3Bloc1;

/*IES Sabadell.
CFGS DAM M03 UF1
Bloc 1 Exercici 10
Descripcio: Algorisme que llegeix dos n�meros enters positius i diferents i ens diu si el major �s m�ltiple del menor,
 o el que �s al mateix, que el menor �s divisor del major. 
Autor: David L�pez 
 */

import java.util.Scanner;

public class Exercici10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner reader = new Scanner(System.in);
		int num1 = 0;
		int num2 = 0;
		
		System.out.print("Introdueix un numero positiu enter: ");
		num1 = reader.nextInt();
		
		System.out.print("Introdueix un numero diferent al primer positiu enter: ");
		num2 = reader.nextInt();
		
		reader.close();
		
		if (num1>num2){
			if (num1%num2==0) {
				System.out.print("El numero "+num1+" �s multiple de "+num2+".");
			}
				else System.out.print("El numero "+num1+" no �s multiple de "+num2+".");
				
		}
		else {
			if (num2>num1) {
				if (num2%num1==0) {
					System.out.print("El numero "+num2+" �s multiple de "+num1+".");
				}
				else System.out.print("El numero "+num2+" no es multiple de "+num1+".");
			}
		}

	}

}
